# Procedures

### Criação de Procedure HelloWorld
```sql
use etec;

delimiter $$

drop procedure if exists HelloWorld$$
create procedure HelloWorld()
begin
	select 'Hello World!';
end$$

call HelloWorld() $$

delimiter ;
```

### Criação de procedure raizquadrada com passagem de argumentos
```sql

delimiter $$

drop procedure if exists raizquadrada$$
create procedure raizquadrada(valor int) -- Passagem de argumento
begin
	-- declaracao de variavel
	declare raiz float;
    set raiz = sqrt(valor);
    select raiz;
end$$

delimiter ;

call raizquadrada(81);
```

### Utilizar variávesi globais para acessar externos dados ao procedure, como se fosse um retorno de função.

```sql
delimiter $$
drop procedure if exists getHoraAtual$$
create procedure getHoraAtual()
begin
	-- Declaracao de variavel global
	set @hora_atual = curtime();
end$$
delimiter ;

call getHoraAtual();

-- Acesso ao conteúdo da variavel atribuido pela funcao.
select @hora_atual;
```

### Uso de procedures aninhados

```sql
delimiter $$

drop procedure if exists parte_do_dia;
create procedure parte_do_dia()
begin
	call getHoraAtual();
	if @hora_atual < '12:00:00' then
		set @parte_do_dia = 'Manhã';
	elseif @hora_atual = '12:00:00' then
		set @parte_do_dia = 'Meio dia';
	else
		set @parte_do_dia = 'Tarde ou noite';
	end if;
    select @parte_do_dia as parte_do_dia;
end$$

call parte_do_dia();
select @parte_do_dia;
```


